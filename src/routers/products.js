const express = require("express");
const {productInfo} = require("../data/products");

function productsRouter() {
    const router = express.Router();
    router.post("/products", (request, response) => {
        console.log(request)
        const id = new Date().getTime();
        const productsData = {...request.body, id}     
        userInfo.push(productsData);
        response.send(productsInfo);
    });
    return router;

}
module.exports = {
    productsRouter
}