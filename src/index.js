const express = require("express");
const { productsRouter } = require("./routers/products");

const {userRouter} = require("./routers/users")

const app = express();

app.use(express.json);

const port = 9000;

app.use("/api/v1", userRouter());

app.use("/api/v1", productsRouter());

app.listen(port,() => {
    console.log("I'm working >:D YEAAH")
})
