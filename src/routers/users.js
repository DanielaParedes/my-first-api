const express = require("express");
const {userInfo} = require("../data/users");

function userRouter() {
    const router = express.Router();
    router.post("/users", (request, response) => {
        console.log(request)
        const id = new Date().getTime();
        const userData = {...request.body, id}     
        userInfo.push(userData);
        response.send(userInfo);
    });
    return router;

}
module.exports = {
    userRouter
}